from typing import Dict
from src.InputReader import InputReader

class Polymer:
    def __init__(self, file):
        self.raw_input = InputReader(file).get_lines()
        self.polymer = list(self.raw_input[0])
        self.pairs_map = self.init_map()
        self.totals = self.init_totals()
        for pair in self.get_initial_pairs():
            self.pairs_map[pair]['count'] += 1
        for letter in self.polymer:
            self.totals[letter] += 1

    def get_initial_pairs(self) -> list:
        return [''.join(self.polymer[n:n + 2]) for n in range(len(self.polymer) - 1)]

    def init_map(self) -> Dict:
        pairs_map = {}
        for part in [x.split() for x in self.raw_input[2:]]:
            pairs_map[part[0]] = {"count": 0, "adds": part[2], "spawns": [part[0][0] + part[2], part[2] + part[0][1]]}
        return pairs_map

    def init_totals(self) -> Dict:
        totals = {}
        for x in set([self.pairs_map[x]['adds'] for x in self.pairs_map]):
            totals[x] = 0
        return totals

    def next_step(self):
        new_pairs = self.init_map()
        for [key, item] in self.pairs_map.items():
            old_pair = self.pairs_map[key]
            self.totals[old_pair['adds']] += old_pair['count']
            new_pairs[item["spawns"][0]]['count'] += old_pair['count']
            new_pairs[item["spawns"][0]]['count'] += old_pair['count']
        self.pairs_map = new_pairs

    def nth_step(self, times: int):
        for _ in range(times):
            self.next_step()

    def result(self) -> int:
        return max(list(self.totals.values())) - min(list(self.totals.values()))

