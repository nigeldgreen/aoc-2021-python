class Lanternfish:
    def __init__(self, input_line: str):
        self.fish = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.populate(input_line)

    def populate(self, input_line: str) -> None:
        for num in [int(i) for i in input_line.split(",")]:
            self.fish[num] = self.fish[num] + 1

    def next(self) -> None:
        spawners = self.fish[0]
        self.fish = self.fish[1:]
        self.fish[6] = self.fish[6] + spawners
        self.fish.append(spawners)
        self.fish

    def after_x_days(self, days: int) -> int:
        for _ in range(days):
            self.next()
        return sum(self.fish)
