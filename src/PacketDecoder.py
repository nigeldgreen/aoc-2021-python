from typing import Dict, List


def transform_to_binary(hex_in):
    decode_table = {
        '0': '0000',
        '1': '0001',
        '2': '0010',
        '3': '0011',
        '4': '0100',
        '5': '0101',
        '6': '0110',
        '7': '0111',
        '8': '1000',
        '9': '1001',
        'A': '1010',
        'B': '1011',
        'C': '1100',
        'D': '1101',
        'E': '1110',
        'F': '1111',
    }
    return "".join([decode_table[h] for h in list(hex_in)])


class PacketDecoder:
    bin: str
    versions: List = []

    def __init__(self, hex_in: str):
        self.bin = transform_to_binary(hex_in)

    def decode(self):
        idx, packet_version, packet_type = self.next_packet_meta(0)

        if packet_type == 4:
            idx = self.get_literal()[0]
        elif self.bin[idx] == '0':
            total_sub_packet_length = int(self.bin[7:22], 2)
            idx += 15
            while total_sub_packet_length > 0:
                # get next valid packet
        else:
            num_sub_packets = int(self.bin[7:18], 2)
            idx += 11
            while num_sub_packets > 0:
                # get next valid packet

    def next_packet_meta(self, idx: int) -> tuple:
        packet_version = int(self.bin[idx:idx + 3], 2)
        idx += 3
        packet_type = int(self.bin[idx:idx + 3], 2)
        idx += 3
        return idx, packet_version, packet_type


    def get_literal(self) -> tuple:
        idx = 6
        output = ""
        reading = True
        while reading:
            output += self.bin[idx + 1:idx + 5]
            if self.bin[idx] == '0':
                reading = False
            idx += 5

        return idx, int(output, 2)

