class Point:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y


class Line:
    def __init__(self, p1: Point, p2: Point):
        self.type = self.set_type(p1, p2)
        self.start, self.end = self.set_points(p1, p2)
        self.points = self.get_points()

    @staticmethod
    def set_type(p1: Point, p2: Point) -> str:
        if p1.y == p2.y:
            return "horizontal"
        elif p1.x == p2.x:
            return "vertical"
        else:
            return "diagonal"

    @staticmethod
    def set_points(p1: Point, p2: Point) -> tuple:
        if p1.y == p2.y:
            return (p1, p2) if p1.x < p2.x else (p2, p1)
        elif p1.x == p2.x:
            return (p1, p2) if p1.y < p2.y else (p2, p1)
        else:
            return (p1, p2) if p1.x < p2.x else (p2, p1)

    def get_points(self) -> list:
        if self.type == "horizontal":
            return [(x, self.start.y) for x in range(self.start.x, self.end.x + 1)]
        elif self.type == "vertical":
            return [(self.start.x, y) for y in range(self.start.y, self.end.y + 1)]
        else:
            if self.start.y < self.end.y:
                ystart = self.start.y
                yend = self.end.y + 1
                yshift = 1
            else:
                ystart = self.start.y
                yend = self.end.y - 1
                yshift = -1
            return list(zip(range(self.start.x, self.end.x + 1), range(ystart, yend, yshift)))

