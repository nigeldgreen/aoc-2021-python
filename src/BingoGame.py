from functools import reduce


class BingoGame:
    def __init__(self, lines):
        self.call_nums = [int(x) for x in lines[0].split(",")][-1::-1]
        self.called = -1
        nums = list(map(lambda x: int(x), reduce(lambda x, y: x + y, map(lambda x: x + " ", lines[2:])).split()))
        self.cards = [nums[x * 25:(x + 1) * 25] for x in range(int(len(nums) / 25))]

    def run_to_first_card(self):
        while self.check_cards() == -1:
            self.call()
        return self.called * self.get_unmarked(self.cards[self.check_cards()])

    def run_to_last_card(self):
        while len(self.cards) > 1:
            self.call()
            while self.check_cards() >= 0:
                self.cards.remove(self.cards[self.check_cards()])
        return self.run_to_first_card()

    def call(self):
        self.called = self.call_nums.pop()
        for idx, card in enumerate(self.cards):
            self.cards[idx] = [x if x != self.called else -1 for x in card]

    def check_cards(self) -> int:
        for idx, card in enumerate(self.cards):
            if self.is_winner(card):
                return idx
        return -1

    @staticmethod
    def get_unmarked(card):
        filtered = [x for x in filter(lambda x: x >= 0, card)]
        return 0 if len(filtered) == 0 else reduce(lambda x, y: x + y, filtered)

    @staticmethod
    def is_winner(card: list) -> bool:
        cols = [reduce(lambda x, y: x + y, l) for l in [card[i::5] for i in range(5)]]
        rows = [reduce(lambda x, y: x + y, l) for l in [card[i * 5:(i + 1) * 5] for i in range(5)]]
        return -5 in cols or -5 in rows
