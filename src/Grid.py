from collections import namedtuple
from functools import reduce
from typing import List
from src.InputReader import InputReader


class Grid:
    _coll: str
    _length: int
    _line_length: int

    @property
    def coll(self):
        return self._coll

    def __init__(self, lines: List):
        self._coll = reduce(lambda x, y: x + y, lines)
        self._length = len(self._coll)
        self._line_length = len(lines[0])

    def __len__(self):
        return self._line_length

    def in_first_column(self, idx: int) -> bool:
        return idx % self._line_length == 0

    def in_last_column(self, idx: int) -> bool:
        return (idx + 1) % self._line_length == 0

    def in_first_row(self, idx: int) -> bool:
        return idx < self._line_length

    def in_last_row(self, idx: int) -> bool:
        return idx >= self._length - self._line_length

    def get_verticals(self, idx):
        i = [idx - len(self), idx + len(self)]
        try:
            return [(x, int(self.coll[x])) for x in i if 0 <= x < self._length]
        except ValueError:
            return [(x, self.coll[x]) for x in i if 0 <= x < self._length]

    def get_horizontals(self, idx):
        i = [idx - 1, idx + 1]
        if self.in_first_column(idx):
            i.remove(idx - 1)
        if self.in_last_column(idx):
            i.remove(idx + 1)
        try:
            return [(x, int(self.coll[x])) for x in i]
        except ValueError:
            return [(x, self.coll[x]) for x in i]

    def get_diagonals(self, idx: int) -> list:
        i = [idx - (len(self) + 1), idx - (len(self) - 1), idx + (len(self) - 1), idx + (len(self) + 1)]
        if self.in_first_column(idx):
            i.remove(idx - (len(self) + 1))
            i.remove(idx + (len(self) - 1))
        elif self.in_last_column(idx):
            i.remove(idx - (len(self) - 1))
            i.remove(idx + (len(self) + 1))
        try:
            return [(x, int(self.coll[x])) for x in i if 0 <= x < self._length]
        except ValueError:
            return [(x, self.coll[x]) for x in i if 0 <= x < self._length]
        
    def get_strict_adjacents(self, idx: int) -> list:
        return sorted(self.get_verticals(idx) + self.get_horizontals(idx))

    def get_all_adjacents(self, idx: int) -> list:
        return sorted(self.get_strict_adjacents(idx) + self.get_diagonals(idx))

    def bottom_and_right(self, idx: int) -> list:
        i = []
        if not self.in_last_column(idx):
            i.append(idx + 1)
        if not self.in_last_row(idx):
            i.append(idx + len(self))
        try:
            return [(x, int(self.coll[x])) for x in sorted(i)]
        except IndexError:
            return [(x, self.coll[x]) for x in sorted(i)]
        except ValueError:
            return [(x, self.coll[x]) for x in sorted(i)]
