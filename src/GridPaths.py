from collections import namedtuple
from typing import Dict, List
from src.Grid import Grid

Path = namedtuple("Path", "idx total")


class GridPaths:
    paths: Dict = {}
    multiplier: int
    grid: Grid
    lowest: int
    rows: int

    def __init__(self, lines: List):
        self.grid = Grid(lines)
        self.max = len(self.grid.coll) - 1
        self.rows = len(lines)
        self.paths = {
            idx: {
                "adj": self.grid.get_strict_adjacents(idx),
                "min": -1
            }
            for idx, elem in enumerate(self.grid.coll)
        }

    def lowest_path_for_index(self, idx: int) -> int:
        lowest = -1
        paths = {(idx, 0)}
        while paths:
            for p in paths:
                if p[0] == self.max and (p[1] < lowest or lowest == -1):
                    lowest = p[1]
                if (p[1] < self.paths[p[0]]['min']) or self.paths[p[0]]['min'] == -1:
                    self.paths[p[0]]['min'] = p[1]
            paths = {
                (a[0], p[1] + a[1])
                for p in paths if (p[1] <= self.paths[p[0]]['min'])
                for a in self.paths[p[0]]['adj']
            }
        print(lowest)
        return lowest
