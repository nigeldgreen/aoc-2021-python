import os
dir_path = os.path.dirname(os.path.realpath(__file__))


class InputReader:
    def __init__(self, file):
        self.file = dir_path + "/../input/" + file
        self.lines = []
        with open(self.file, "r") as lines:
            self.lines = [line.strip() for line in lines]

    def get_lines(self):
        return self.lines

    def get_lines_as_ints(self):
        return [int(x) for x in self.lines]
