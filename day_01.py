from functools import reduce
from src.InputReader import InputReader

reader = InputReader('day_01.txt')


def is_deeper(first: int, last: int) -> bool:
    return first < last


def count_increasing_steps(vals: list) -> int:
    return len([1 for x in range(len(vals) - 1) if is_deeper(vals[x], vals[x + 1])])


def get_triples(vals: list) -> list:
    return [(vals[x], vals[x + 1], vals[x + 2]) for x in range(len(vals) - 2)]


def get_reduced_triples(triples: list) -> list:
    return [reduce(lambda x, y: x + y, triple) for triple in triples]


if __name__ == "__main__":
    vals = reader.get_lines_as_ints()
    print("Part one: ", count_increasing_steps(vals))
    print("Part two: ", count_increasing_steps(get_reduced_triples(get_triples(vals))))
