def part_one(file: str) -> int:
    return -1


def part_two(file: str) -> int:
    return -1


if __name__ == "__main__":
    print(part_one("day_16.txt"))
    print(part_two("day_16.txt"))
