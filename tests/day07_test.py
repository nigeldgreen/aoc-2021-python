import pytest
from src.InputReader import InputReader
from day_07 import get_crabs, calculate_basic_cost, part_one, part_two


def test_can_get_crabs():
    assert {16: 1, 1: 2, 2: 3, 0: 1, 4: 1, 7: 1, 14: 1} == get_crabs('day_07_test.txt')


def test_can_calculate_fuel_for_move():
    crabs = get_crabs('day_07_test.txt')
    assert 41 == calculate_basic_cost(crabs, 1)
    assert 39 == calculate_basic_cost(crabs, 3)
    assert 71 == calculate_basic_cost(crabs, 10)
    assert 37 == calculate_basic_cost(crabs, 2)


def test_part_one():
    crabs = get_crabs('day_07_test.txt')
    assert 37 == part_one(crabs)


def test_part_two():
    crabs = get_crabs('day_07_test.txt')
    assert 168 == part_two(crabs)

