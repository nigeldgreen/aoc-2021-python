from src.InputReader import InputReader
from day_05 import parse_lines, part_one, part_two

lines = parse_lines('day_05_test.txt')


def test_can_get_lines_from_input():
    assert lines[0].start.x == 0 and lines[0].start.y == 9 and lines[0].end.x == 5 and lines[0].end.y == 9
    assert lines[1].start.x == 0 and lines[1].start.y == 8 and lines[1].end.x == 8 and lines[1].end.y == 0
    assert lines[2].start.x == 3 and lines[2].start.y == 4 and lines[2].end.x == 9 and lines[2].end.y == 4
    assert lines[3].start.x == 2 and lines[3].start.y == 1 and lines[3].end.x == 2 and lines[3].end.y == 2
    assert lines[4].start.x == 7 and lines[4].start.y == 0 and lines[4].end.x == 7 and lines[4].end.y == 4
    assert lines[5].start.x == 2 and lines[5].start.y == 0 and lines[5].end.x == 6 and lines[5].end.y == 4
    assert lines[6].start.x == 0 and lines[6].start.y == 9 and lines[6].end.x == 2 and lines[6].end.y == 9
    assert lines[7].start.x == 1 and lines[7].start.y == 4 and lines[7].end.x == 3 and lines[7].end.y == 4
    assert lines[8].start.x == 0 and lines[8].start.y == 0 and lines[8].end.x == 8 and lines[8].end.y == 8
    assert lines[9].start.x == 5 and lines[9].start.y == 5 and lines[9].end.x == 8 and lines[9].end.y == 2


def test_can_get_points_for_lines_in_input():
    assert lines[0].get_points() == [(0, 9), (1, 9), (2, 9), (3, 9), (4, 9), (5, 9)]
    assert lines[1].get_points() == [(0, 8), (1, 7), (2, 6), (3, 5), (4, 4), (5, 3), (6, 2), (7, 1), (8, 0)]


def test_can_get_answer_to_part_one():
    assert 5 == part_one(lines)


def test_can_get_answer_to_part_two():
    assert 12 == part_two(lines)
