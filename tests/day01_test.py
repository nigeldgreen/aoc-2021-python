from day_01 import is_deeper, count_increasing_steps, get_triples, get_reduced_triples
from src.InputReader import InputReader

reader = InputReader('day_01_test.txt')
vals = reader.get_lines_as_ints()


def test_can_read_lines_of_input_as_integers():
    assert vals == [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]


def test_first_is_deeper_than_last():
    assert is_deeper(1, 2) is True


def test_first_is_not_deeper_than_last():
    assert is_deeper(2, 1) is False


def test_first_is_not_deeper_than_last_when_equal():
    assert is_deeper(1, 1) is False


def test_correct_steps_for_part_one():
    assert count_increasing_steps(vals) == 7


def test_can_get_triples():
    assert get_triples(vals) == [
        (199, 200, 208),
        (200, 208, 210),
        (208, 210, 200),
        (210, 200, 207),
        (200, 207, 240),
        (207, 240, 269),
        (240, 269, 260),
        (269, 260, 263)
    ]


def test_can_get_reduced_triples():
    triples = get_triples(vals)
    assert get_reduced_triples(triples) == [
        607,
        618,
        618,
        617,
        647,
        716,
        769,
        792
    ]


def test_correct_steps_for_part_two():
    assert count_increasing_steps(get_reduced_triples(get_triples(vals))) == 5
