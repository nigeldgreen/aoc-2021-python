import day_02
from src.InputReader import InputReader
from day_02 import split_commands, get_raw_result, get_result_with_aim

reader = InputReader('day_02_test.txt')
vals = reader.get_lines()


def test_can_get_commands_from_input():
    assert split_commands(vals) == [
        ("forward", 5),
        ("down", 5),
        ("forward", 8),
        ("up", 3),
        ("down", 8),
        ("forward", 2),
    ]


def test_can_get_correct_raw_result():
    assert get_raw_result(split_commands(vals)) == 150


def test_can_get_correct_result_with_aim():
    assert get_result_with_aim(split_commands(vals)) == 900
