from src.PointsAndLines import Point, Line


def test_can_create_new_points():
    p1 = Point(25, 42)
    assert p1.x == 25 and p1.y == 42

    p2 = Point(0, 0)
    assert p2.x == 0 and p2.y == 0

    p3 = Point(-10, 10)
    assert p3.x == -10 and p3.y == 10


def test_can_create_new_lines():
    l1 = Line(Point(0, 4), Point(0, 8))
    assert l1.start.x == 0 and l1.start.y == 4
    assert l1.end.x == 0 and l1.end.y == 8
    assert l1.type == "vertical"

    l2 = Line(Point(0, 0), Point(-3, -3))
    assert l2.start.x == -3 and l2.start.y == -3
    assert l2.end.x == 0 and l2.end.y == 0
    assert l2.type == "diagonal"

    l3 = Line(Point(-3, 42), Point(-7, 42))
    assert l3.start.x == -7 and l3.start.y == 42
    assert l3.end.x == -3 and l3.end.y == 42
    assert l3.type == "horizontal"


def test_can_get_all_points_on_a_line():
    l1 = Line(Point(0, 4), Point(0, 8))
    assert l1.points == [(0, 4), (0, 5), (0, 6), (0, 7), (0, 8)]

    l2 = Line(Point(0, 0), Point(-3, -3))
    assert l2.points == [(-3, -3), (-2, -2), (-1, -1), (0, 0)]

    l3 = Line(Point(-3, 42), Point(-7, 42))
    assert l3.points == [(-7, 42), (-6, 42), (-5, 42), (-4, 42), (-3, 42)]

    l4 = Line(Point(0, 0), Point(-3, -3))
    assert l4.points == [(-3, -3), (-2, -2), (-1, -1), (0, 0)]

    l5 = Line(Point(-1, 3), Point(2, 0))
    assert l5.points == [(-1, 3), (0, 2), (1, 1), (2, 0)]
