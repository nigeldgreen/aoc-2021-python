from src.InputReader import InputReader


def test_can_get_input_as_list_of_strings():
    ir = InputReader('day_01_test.txt')
    assert ir.get_lines() == [
        "199",
        "200",
        "208",
        "210",
        "200",
        "207",
        "240",
        "269",
        "260",
        "263"
    ]


def test_can_get_list_of_strings_with_leading_zeroes():
    ir = InputReader('day_03_test.txt')
    assert ir.get_lines() == [
        "00100",
        "11110",
        "10110",
        "10111",
        "10101",
        "01111",
        "00111",
        "11100",
        "10000",
        "11001",
        "00010",
        "01010",
    ]


def test_can_get_input_as_list_of_ints():
    ir = InputReader('day_01_test.txt')
    assert ir.get_lines_as_ints() == [
        199,
        200,
        208,
        210,
        200,
        207,
        240,
        269,
        260,
        263
    ]
