from src.Polymer import Polymer

pairs_map_0 = {
    "BB": {"count": 0, "adds": "N", "spawns": ["BN", "NB"]},
    "BC": {"count": 0, "adds": "B", "spawns": ["BB", "BC"]},
    "BH": {"count": 0, "adds": "H", "spawns": ["BH", "HH"]},
    "BN": {"count": 0, "adds": "B", "spawns": ["BB", "BN"]},
    "CB": {"count": 1, "adds": "H", "spawns": ["CH", "HB"]},
    "CC": {"count": 0, "adds": "N", "spawns": ["CN", "NC"]},
    "CH": {"count": 0, "adds": "B", "spawns": ["CB", "BH"]},
    "CN": {"count": 0, "adds": "C", "spawns": ["CC", "CN"]},
    "HB": {"count": 0, "adds": "C", "spawns": ["HC", "CB"]},
    "HC": {"count": 0, "adds": "B", "spawns": ["HB", "BC"]},
    "HH": {"count": 0, "adds": "N", "spawns": ["HN", "NH"]},
    "HN": {"count": 0, "adds": "C", "spawns": ["HC", "CN"]},
    "NB": {"count": 0, "adds": "B", "spawns": ["NB", "BB"]},
    "NC": {"count": 1, "adds": "B", "spawns": ["NB", "BC"]},
    "NH": {"count": 0, "adds": "C", "spawns": ["NC", "CH"]},
    "NN": {"count": 1, "adds": "C", "spawns": ["NC", "CN"]},
}
pairs_map_1 = {
    "BB": {"count": 0, "adds": "N", "spawns": ["BN", "NB"]},
    "BC": {"count": 1, "adds": "B", "spawns": ["BB", "BC"]},
    "BH": {"count": 0, "adds": "H", "spawns": ["BH", "HH"]},
    "BN": {"count": 0, "adds": "B", "spawns": ["BB", "BN"]},
    "CB": {"count": 0, "adds": "H", "spawns": ["CH", "HB"]},
    "CC": {"count": 0, "adds": "N", "spawns": ["CN", "NC"]},
    "CH": {"count": 1, "adds": "B", "spawns": ["CB", "BH"]},
    "CN": {"count": 1, "adds": "C", "spawns": ["CC", "CN"]},
    "HB": {"count": 1, "adds": "C", "spawns": ["HC", "CB"]},
    "HC": {"count": 0, "adds": "B", "spawns": ["HB", "BC"]},
    "HH": {"count": 0, "adds": "N", "spawns": ["HN", "NH"]},
    "HN": {"count": 0, "adds": "C", "spawns": ["HC", "CN"]},
    "NB": {"count": 1, "adds": "B", "spawns": ["NB", "BB"]},
    "NC": {"count": 1, "adds": "B", "spawns": ["NB", "BC"]},
    "NH": {"count": 0, "adds": "C", "spawns": ["NC", "CH"]},
    "NN": {"count": 0, "adds": "C", "spawns": ["NC", "CN"]}
}
pairs_map_2 = {
    "BB": {"count": 2, "adds": "N", "spawns": ["BN", "NB"]},
    "BC": {"count": 2, "adds": "B", "spawns": ["BB", "BC"]},
    "BH": {"count": 1, "adds": "H", "spawns": ["BH", "HH"]},
    "BN": {"count": 0, "adds": "B", "spawns": ["BB", "BN"]},
    "CB": {"count": 2, "adds": "H", "spawns": ["CH", "HB"]},
    "CC": {"count": 1, "adds": "N", "spawns": ["CN", "NC"]},
    "CH": {"count": 0, "adds": "B", "spawns": ["CB", "BH"]},
    "CN": {"count": 1, "adds": "C", "spawns": ["CC", "CN"]},
    "HB": {"count": 0, "adds": "C", "spawns": ["HC", "CB"]},
    "HC": {"count": 1, "adds": "B", "spawns": ["HB", "BC"]},
    "HH": {"count": 0, "adds": "N", "spawns": ["HN", "NH"]},
    "HN": {"count": 0, "adds": "C", "spawns": ["HC", "CN"]},
    "NB": {"count": 2, "adds": "B", "spawns": ["NB", "BB"]},
    "NC": {"count": 0, "adds": "B", "spawns": ["NB", "BC"]},
    "NH": {"count": 0, "adds": "C", "spawns": ["NC", "CH"]},
    "NN": {"count": 0, "adds": "C", "spawns": ["NC", "CN"]}
}


def test_can_create_new_polymer():
    poly = Polymer("day_14_test.txt")
    assert poly.polymer == ["N", "N", "C", "B"]


def test_can_get_pairs_for_initial_polymer():
    poly = Polymer("day_14_test.txt")
    assert poly.get_initial_pairs() == ["NN", "NC", "CB"]
    poly.polymer = ["A", "B", "C", "D", "E", "F"]
    assert poly.get_initial_pairs() == ["AB", "BC", "CD", "DE", "EF"]


def test_can_get_correct_rule_map():
    poly = Polymer("day_14_test.txt")
    assert poly.pairs_map == pairs_map_0
    assert poly.totals == {"B": 1, "C": 1, "H": 0, "N": 2}


def test_can_get_correct_new_pairs_at_each_step():
    poly = Polymer("day_14_test.txt")
    poly.next_step()
    assert poly.pairs_map == pairs_map_1
    poly.next_step()
    assert poly.pairs_map == pairs_map_2


def test_can_get_correct_new_totals_at_each_step():
    poly = Polymer("day_14_test.txt")
    poly.next_step()
    assert poly.totals == {"B": 2, "C": 2, "H": 1, "N": 2}
    poly.next_step()
    assert poly.totals == {"B": 6, "C": 4, "H": 1, "N": 2}


def test_can_get_correct_nth_steps():
    poly = Polymer("day_14_test.txt")
    poly.nth_step(1)
    assert poly.pairs_map == pairs_map_1
    assert poly.totals == {"B": 2, "C": 2, "H": 1, "N": 2}
    poly.nth_step(1)
    assert poly.pairs_map == pairs_map_2
    assert poly.totals == {"B": 6, "C": 4, "H": 1, "N": 2}


def test_results():
    poly = Polymer("day_14_test.txt")
    poly.nth_step(10)
    assert poly.result() == 1588
    poly.nth_step(30)
    assert poly.result() == 2188189693529
