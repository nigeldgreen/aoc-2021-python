from src.Grid import Grid
from src.InputReader import InputReader

l1 = InputReader("collection_test_1.txt").get_lines()
grid1 = Grid(l1)
l2 = InputReader("collection_test_2.txt").get_lines()
grid2 = Grid(l2)


def test_can_get_new_list_from_input():
    assert grid1.coll == '123456789'
    assert len(grid1) == 3
    assert grid2.coll == 'abcdefghijklmnopqrstuvwxy'
    assert len(grid2) == 5


def test_can_identify_first_column():
    assert grid1.in_first_column(0)
    assert grid1.in_first_column(6)
    assert not grid1.in_first_column(1)
    assert not grid1.in_first_column(5)
    assert grid2.in_first_column(5)
    assert grid2.in_first_column(15)
    assert not grid2.in_first_column(3)
    assert not grid2.in_first_column(21)


def test_identify_last_column():
    assert grid1.in_last_column(2)
    assert grid1.in_last_column(8)
    assert not grid1.in_last_column(0)
    assert not grid1.in_last_column(1)
    assert not grid1.in_last_column(3)
    assert grid2.in_last_column(4)
    assert grid2.in_last_column(24)
    assert not grid2.in_last_column(3)
    assert not grid2.in_last_column(17)


def test_identify_first_row():
    assert grid1.in_first_row(0)
    assert grid1.in_first_row(2)
    assert not grid1.in_first_row(6)
    assert not grid1.in_first_row(8)
    assert grid2.in_first_row(0)
    assert grid2.in_first_row(4)
    assert not grid2.in_first_row(5)
    assert not grid2.in_first_row(25)


def test_identify_last_row():
    assert not grid1.in_last_row(0)
    assert grid1.in_last_row(6)
    assert grid1.in_last_row(8)
    assert not grid2.in_last_row(6)
    assert grid2.in_last_row(21)
    assert grid2.in_last_row(25)


def test_get_strict_adjacents_test():
    assert grid1.get_strict_adjacents(0) == [(1, 2), (3, 4)]
    assert grid1.get_strict_adjacents(1) == [(0, 1), (2, 3), (4, 5)]
    assert grid1.get_strict_adjacents(2) == [(1, 2), (5, 6)]
    assert grid1.get_strict_adjacents(3) == [(0, 1), (4, 5), (6, 7)]
    assert grid1.get_strict_adjacents(4) == [(1, 2), (3, 4), (5, 6), (7, 8)]
    assert grid1.get_strict_adjacents(5) == [(2, 3), (4, 5), (8, 9)]
    assert grid1.get_strict_adjacents(6) == [(3, 4), (7, 8)]
    assert grid1.get_strict_adjacents(7) == [(4, 5), (6, 7), (8, 9)]
    assert grid1.get_strict_adjacents(8) == [(5, 6), (7, 8)]

    assert grid2.get_strict_adjacents(0) == [(1, 'b'), (5, 'f')]
    assert grid2.get_strict_adjacents(4) == [(3, 'd'), (9, 'j')]
    assert grid2.get_strict_adjacents(6) == [(1, 'b'), (5, 'f'), (7, 'h'), (11, 'l')]
    assert grid2.get_strict_adjacents(11) == [(6, 'g'), (10, 'k'), (12, 'm'), (16, 'q')]
    assert grid2.get_strict_adjacents(18) == [(13, 'n'), (17, 'r'), (19, 't'), (23, 'x')]
    assert grid2.get_strict_adjacents(20) == [(15, 'p'), (21, 'v')]


def test_get_all_adjacents_test():
    assert grid1.get_all_adjacents(0) == [(1, 2), (3, 4), (4, 5)]
    assert grid1.get_all_adjacents(1) == [(0, 1), (2, 3), (3, 4), (4, 5), (5, 6)]
    assert grid1.get_all_adjacents(2) == [(1, 2), (4, 5), (5, 6)]
    assert grid1.get_all_adjacents(3) == [(0, 1), (1, 2), (4, 5), (6, 7), (7, 8)]
    assert grid1.get_all_adjacents(4) == [(0, 1), (1, 2), (2, 3), (3, 4), (5, 6), (6, 7), (7, 8), (8, 9)]
    assert grid1.get_all_adjacents(5) == [(1, 2), (2, 3), (4, 5), (7, 8), (8, 9)]
    assert grid1.get_all_adjacents(6) == [(3, 4), (4, 5), (7, 8)]
    assert grid1.get_all_adjacents(7) == [(3, 4), (4, 5), (5, 6), (6, 7), (8, 9)]
    assert grid1.get_all_adjacents(8) == [(4, 5), (5, 6), (7, 8)]

    assert grid2.get_all_adjacents(2) == [(1, 'b'), (3, 'd'), (6, 'g'), (7, 'h'), (8, 'i')]
    assert grid2.get_all_adjacents(4) == [(3, 'd'), (8, 'i'), (9, 'j')]
    assert grid2.get_all_adjacents(11) == [(5, 'f'), (6, 'g'), (7, 'h'), (10, 'k'), (12, 'm'), (15, 'p'), (16, 'q'), (17, 'r')]
    assert grid2.get_all_adjacents(15) == [(10, 'k'), (11, 'l'), (16, 'q'), (20, 'u'), (21, 'v')]
    assert grid2.get_all_adjacents(17) == [(11, 'l'), (12, 'm'), (13, 'n'), (16, 'q'), (18, 's'), (21, 'v'), (22, 'w'), (23, 'x')]
    assert grid2.get_all_adjacents(21) == [(15, 'p'), (16, 'q'), (17, 'r'), (20, 'u'), (22, 'w')]
    assert grid2.get_all_adjacents(24) == [(18, 's'), (19, 't'), (23, 'x')]


def test_get_bottom_and_right():
    assert grid1.bottom_and_right(0) == [(1, 2), (3, 4)]
    assert grid1.bottom_and_right(1) == [(2, 3), (4, 5)]
    assert grid1.bottom_and_right(2) == [(5, 6)]
    assert grid1.bottom_and_right(3) == [(4, 5), (6, 7)]
    assert grid1.bottom_and_right(4) == [(5, 6), (7, 8)]
    assert grid1.bottom_and_right(5) == [(8, 9)]
    assert grid1.bottom_and_right(6) == [(7, 8)]
    assert grid1.bottom_and_right(7) == [(8, 9)]
    assert grid1.bottom_and_right(8) == []

    assert grid2.bottom_and_right(0) == [(1, 'b'), (5, 'f')]
    assert grid2.bottom_and_right(4) == [(9, 'j')]
    assert grid2.bottom_and_right(6) == [(7, 'h'), (11, 'l')]
    assert grid2.bottom_and_right(11) == [(12, 'm'), (16, 'q')]
    assert grid2.bottom_and_right(18) == [(19, 't'), (23, 'x')]
    assert grid2.bottom_and_right(20) == [(21, 'v')]
