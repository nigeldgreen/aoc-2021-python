from src.InputReader import InputReader
from day_03 import split_input, get_rate, get_filtered_rate

reader = InputReader('day_03_test.txt')
vals = reader.get_lines()


def test_can_get_array_of_digits_from_input():
    assert split_input(vals) == [
        ["0", "0", "1", "0", "0"],
        ["1", "1", "1", "1", "0"],
        ["1", "0", "1", "1", "0"],
        ["1", "0", "1", "1", "1"],
        ["1", "0", "1", "0", "1"],
        ["0", "1", "1", "1", "1"],
        ["0", "0", "1", "1", "1"],
        ["1", "1", "1", "0", "0"],
        ["1", "0", "0", "0", "0"],
        ["1", "1", "0", "0", "1"],
        ["0", "0", "0", "1", "0"],
        ["0", "1", "0", "1", "0"],
    ]


def test_can_get_gamma_rate():
    assert get_rate("gamma", vals) == 22


def test_can_get_epsilon_rate():
    assert get_rate("epsilon", vals) == 9


def test_can_get_oxygen_generator_rate():
    assert get_filtered_rate("oxygen", vals) == 23


def test_can_get_co2_scrubber_rate():
    assert get_filtered_rate("CO2", vals) == 10
