from src.BingoGame import BingoGame
from src.InputReader import InputReader

reader = InputReader('day_04_test.txt')
b_game = BingoGame(reader.get_lines())


def test_can_get_call_nums_for_bingo_game():
    assert b_game.call_nums == [1, 26, 3, 19, 8, 20, 18, 22, 12, 25, 15, 6, 13, 16, 10, 24, 21, 14, 0, 2, 23, 17, 11, 5, 9, 4, 7]


def test_can_get_cards_from_input():
    assert b_game.cards == [
        [22, 13, 17, 11, 0, 8, 2, 23, 4, 24, 21, 9, 14, 16, 7, 6, 10, 3, 18, 5, 1, 12, 20, 15, 19],
        [3, 15, 0, 2, 22, 9, 18, 13, 17, 5, 19, 8, 7, 25, 23, 20, 11, 10, 24, 4, 14, 21, 16, 12, 6],
        [14, 21, 17, 24, 4, 10, 16, 15, 9, 19, 18, 8, 23, 26, 20, 22, 11, 13, 6, 5, 2, 0, 12, 3, 7]
    ]


def test_winners():
    no_win = [22, 13, 17, 11, 0, 8, 2, 23, 4, 24, 21, 9, 14, 16, 7, 6, 10, 3, 18, 5, 1, 12, 20, 15, 19]
    winning_col1 = [-1, 13, 17, 11, 0, -1, 2, 23, 4, 24, -1, 9, 14, 16, 7, -1, 10, 3, 18, 5, -1, 12, 20, 15, 19]
    winning_row1 = [22, 13, 17, 11, 0, -1, -1, -1, -1, -1, 21, 9, 14, 16, 7, 6, 10, 3, 18, 5, 1, 12, 20, 15, 19]
    winning_col2 = [22, 13, -1, 11, 0, 8, 2, -1, 4, 24, 21, 9, -1, 16, 7, 6, 10, -1, 18, 5, 1, 12, -1, 15, 19]
    winning_row2 = [22, 13, 17, 11, 0, 8, 2, 23, 4, 24, 21, 9, 14, 16, 7, 6, 10, 3, 18, 5, -1, -1, -1, -1, -1]
    assert b_game.is_winner(no_win) is False
    assert b_game.is_winner(winning_col1) is True
    assert b_game.is_winner(winning_col2) is True
    assert b_game.is_winner(winning_row1) is True
    assert b_game.is_winner(winning_row1) is True


def test_can_get_card_sum():
    assert b_game.get_unmarked([1, 2, 3, 4]) == 10
    assert b_game.get_unmarked([-1, -1, -1, -1]) == 0
    assert b_game.get_unmarked([1, -1, -1, 4]) == 5
    assert b_game.get_unmarked([-1, -1, -1, 42]) == 42


def test_correct_result_after_3_calls():
    b_game.call()
    assert b_game.cards == [
        [22, 13, 17, 11, 0, 8, 2, 23, 4, 24, 21, 9, 14, 16, -1, 6, 10, 3, 18, 5, 1, 12, 20, 15, 19],
        [3, 15, 0, 2, 22, 9, 18, 13, 17, 5, 19, 8, -1, 25, 23, 20, 11, 10, 24, 4, 14, 21, 16, 12, 6],
        [14, 21, 17, 24, 4, 10, 16, 15, 9, 19, 18, 8, 23, 26, 20, 22, 11, 13, 6, 5, 2, 0, 12, 3, -1]
    ]
    b_game.call()
    assert b_game.cards == [
        [22, 13, 17, 11, 0, 8, 2, 23, -1, 24, 21, 9, 14, 16, -1, 6, 10, 3, 18, 5, 1, 12, 20, 15, 19],
        [3, 15, 0, 2, 22, 9, 18, 13, 17, 5, 19, 8, -1, 25, 23, 20, 11, 10, 24, -1, 14, 21, 16, 12, 6],
        [14, 21, 17, 24, -1, 10, 16, 15, 9, 19, 18, 8, 23, 26, 20, 22, 11, 13, 6, 5, 2, 0, 12, 3, -1]
    ]
    b_game.call()
    assert b_game.cards == [
        [22, 13, 17, 11, 0, 8, 2, 23, -1, 24, 21, -1, 14, 16, -1, 6, 10, 3, 18, 5, 1, 12, 20, 15, 19],
        [3, 15, 0, 2, 22, -1, 18, 13, 17, 5, 19, 8, -1, 25, 23, 20, 11, 10, 24, -1, 14, 21, 16, 12, 6],
        [14, 21, 17, 24, -1, 10, 16, 15, -1, 19, 18, 8, 23, 26, 20, 22, 11, 13, 6, 5, 2, 0, 12, 3, -1]
    ]


def test_correct_result_for_first_winning_card():
    assert b_game.run_to_first_card() == 4512


def test_correct_result_for_last_winning_card():
    assert b_game.run_to_last_card() == 1924
