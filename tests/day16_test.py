import pytest
from day_16 import part_one, part_two
from src.PacketDecoder import PacketDecoder


def test_correct_result_for_literal():
    d = PacketDecoder("D2FE28")
    assert d.decode() == 2021


def test_correct_result_for_operator():
    # d = PacketDecoder("38006F45291200")
    # assert d.sub_packets == ['11010001010', '0101001000100100']
    d = PacketDecoder("EE00D40C823060")
    assert d.sub_packets == ['01010000001', '10010000010', '00110000011']


def test_part_one():
    assert part_one("day_15_test.txt") == -1


def test_part_two():
    assert part_two("day_15_test.txt") == -1
