from src.InputReader import InputReader
from src.Lanternfish import Lanternfish

input_line = InputReader('day_06_test.txt').get_lines()[0]


def test_can_create_lanternfish():
    f1 = Lanternfish('1, 2, 3, 4, 5')
    assert f1.fish == [0, 1, 1, 1, 1, 1, 0, 0, 0]
    f2 = Lanternfish('0, 0, 0, 0, 0, 0')
    assert f2.fish == [6, 0, 0, 0, 0, 0, 0, 0, 0]
    f3 = Lanternfish('8, 8, 8, 8, 7, 7, 7, 6, 6, 5')
    assert f3.fish == [0, 0, 0, 0, 0, 1, 2, 3, 4]
    f3 = Lanternfish(input_line)
    assert f3.fish == [0, 1, 1, 2, 1, 0, 0, 0, 0]


def test_first_18_days():
    lfish = Lanternfish(input_line)
    assert lfish.fish == [0, 1, 1, 2, 1, 0, 0, 0, 0]

    lfish.next()
    assert lfish.fish == [1, 1, 2, 1, 0, 0, 0, 0, 0]
    lfish.next()
    assert lfish.fish == [1, 2, 1, 0, 0, 0, 1, 0, 1]
    lfish.next()
    assert lfish.fish == [2, 1, 0, 0, 0, 1, 1, 1, 1]
    lfish.next()
    assert lfish.fish == [1, 0, 0, 0, 1, 1, 3, 1, 2]
    lfish.next()
    assert lfish.fish == [0, 0, 0, 1, 1, 3, 2, 2, 1]
    lfish.next()
    assert lfish.fish == [0, 0, 1, 1, 3, 2, 2, 1, 0]
    lfish.next()
    assert lfish.fish == [0, 1, 1, 3, 2, 2, 1, 0, 0]
    lfish.next()
    assert lfish.fish == [1, 1, 3, 2, 2, 1, 0, 0, 0]
    lfish.next()
    assert lfish.fish == [1, 3, 2, 2, 1, 0, 1, 0, 1]
    lfish.next()
    assert lfish.fish == [3, 2, 2, 1, 0, 1, 1, 1, 1]
    lfish.next()
    assert lfish.fish == [2, 2, 1, 0, 1, 1, 4, 1, 3]
    lfish.next()
    assert lfish.fish == [2, 1, 0, 1, 1, 4, 3, 3, 2]
    lfish.next()
    assert lfish.fish == [1, 0, 1, 1, 4, 3, 5, 2, 2]
    lfish.next()
    assert lfish.fish == [0, 1, 1, 4, 3, 5, 3, 2, 1]
    lfish.next()
    assert lfish.fish == [1, 1, 4, 3, 5, 3, 2, 1, 0]
    lfish.next()
    assert lfish.fish == [1, 4, 3, 5, 3, 2, 2, 0, 1]
    lfish.next()
    assert lfish.fish == [4, 3, 5, 3, 2, 2, 1, 1, 1]
    lfish.next()
    assert lfish.fish == [3, 5, 3, 2, 2, 1, 5, 1, 4]


def test_part_one():
    lfish = Lanternfish(input_line)
    assert 5934 == lfish.after_x_days(80)


def test_part_two():
    lfish = Lanternfish(input_line)
    assert 26984457539 == lfish.after_x_days(256)
