import cProfile
from typing import List

from src.GridPaths import GridPaths
from src.InputReader import InputReader


def part_one(file: str) -> int:
    lines = InputReader(file).get_lines()
    g = GridPaths(lines)
    return g.lowest_path_for_index(0)


def part_two(file: str) -> int:
    lines = InputReader(file).get_lines()
    g = GridPaths(extend_grid(lines))
    return g.lowest_path_for_index(0)


def extended_grid(file: str) -> str:
    pass


def increment(i: int, inc: int = 1) -> int:
    new_val = i + inc
    return new_val % 9 if new_val != 9 else new_val


def increment_line(line: str, inc: int = 1) -> str:
    nums = [int(n) for n in line]
    return "".join([str(increment(n, inc)) for n in nums])


def extend_line(line: str) -> str:
    temp = line
    for _ in range(4):
        temp = increment_line(temp)
        line = line + temp
    return line


def extend_grid(lines: List) -> List:
    result = [extend_line(line) for line in lines]
    for inc in range(1, 5):
        new_lines = [extend_line(increment_line(l, inc)) for l in lines]
        result.extend(new_lines)
    return result


if __name__ == "__main__":
    cProfile.run('part_two("day_15.txt")')
    # print(part_one("day_15.txt"))
    # print(part_two("day_15.txt"))
