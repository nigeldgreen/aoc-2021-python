from src.InputReader import InputReader

reader = InputReader('day_02.txt')


def split_commands(lines: list) -> list:
    return [(x.split()[0], int(x.split()[1])) for x in lines]


def get_raw_result(commands: list) -> int:
    hpos = 0
    depth = 0
    for [cmd, val] in commands:
        if cmd == "forward":
            hpos = hpos + val
        elif cmd == "down":
            depth = depth + val
        else:
            depth = depth - val

    return hpos * depth


def get_result_with_aim(commands: list) -> int:
    hpos = 0
    depth = 0
    aim = 0
    for [cmd, val] in commands:
        if cmd == "forward":
            hpos = hpos + val
            depth = depth + (aim * val)
        elif cmd == "down":
            aim = aim + val
        else:
            aim = aim - val

    return hpos * depth


if __name__ == "__main__":
    vals = reader.get_lines()
    print("Part one: ", get_raw_result(split_commands(vals)))
    print("Part two: ", get_result_with_aim(split_commands(vals)))
