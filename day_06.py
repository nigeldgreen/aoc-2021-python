from src.Lanternfish import Lanternfish
from src.InputReader import InputReader


if __name__ == "__main__":
    input_line = InputReader('day_06.txt').get_lines()[0]
    lfish = Lanternfish(input_line)
    print(lfish.after_x_days(80))
    lfish = Lanternfish(input_line)
    print(lfish.after_x_days(256))

