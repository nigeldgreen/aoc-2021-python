from collections import Counter
from typing import Dict
from src.InputReader import InputReader


def get_crabs(file: str) -> Dict:
    input_line = InputReader(file).get_lines()[0]
    return Counter([int(num) for num in input_line.split(",")])


def calculate_basic_cost(crabs: Dict, shift: int) -> int:
    return sum([qty * abs(num - shift) for num, qty in crabs.items()])


def part_one(crabs: Dict) -> int:
    vals = [calculate_basic_cost(crabs, x) for x in range(max(crabs) + 1)]
    return min(vals)


def calculate_sum_of_ints(num: int, shift: int) -> int:
    val = abs(num - shift)
    return int((val * (val + 1)) / 2)


def calculate_compound_cost(crabs: Dict, shift: int) -> int:
    return sum([qty * calculate_sum_of_ints(num, shift) for num, qty in crabs.items()])


def part_two(crabs: Dict) -> int:
    vals = [calculate_compound_cost(crabs, x) for x in range(max(crabs) + 1)]
    return min(vals)


if __name__ == "__main__":
    c = get_crabs('day_07.txt')
    print(part_one(c))
    print(part_two(c))
