from src.Polymer import Polymer


def part_one():
    poly1 = Polymer("day_14.txt")
    poly1.nth_step(10)
    print("After 10 steps the result is: ", poly1.result())


def part_two():
    poly2 = Polymer("day_14.txt")
    poly2.nth_step(40)
    print("After 40 steps the result is: ", poly2.result())


if __name__ == "__main__":
    part_one()
    part_two()
