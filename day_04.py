from src.BingoGame import BingoGame
from src.InputReader import InputReader

if __name__ == "__main__":
    reader = InputReader('day_04.txt')
    print("Part one: ", BingoGame(reader.get_lines()).run_to_first_card())
    print("Part two: ", BingoGame(reader.get_lines()).run_to_last_card())
