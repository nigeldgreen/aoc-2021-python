from functools import reduce
from collections import Counter
from src.PointsAndLines import Line, Point
from src.InputReader import InputReader


def parse_lines(file: str) -> list:
    lines = []
    for line in [line.split(' ') for line in InputReader(file).get_lines()]:
        p1 = line[0].split(',')
        p2 = line[2].split(',')
        lines.append(Line(Point(int(p1[0]), int(p1[1])), Point(int(p2[0]), int(p2[1]))))
    return lines


def part_one(lines: list):
    points = reduce(lambda x, y: x + y,
                    [line.get_points() for line in lines if line.type != "diagonal"])
    return len([v for v in Counter(points).values() if v >= 2])


def part_two(lines: list):
    points = reduce(lambda x, y: x + y, [line.get_points() for line in lines])
    return len([v for v in Counter(points).values() if v >= 2])


if __name__ == "__main__":
    print(part_one(parse_lines('day_05.txt')))
    print(part_two(parse_lines('day_05.txt')))
