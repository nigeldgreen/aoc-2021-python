from src.InputReader import InputReader
from collections import Counter

reader = InputReader('day_03.txt')


def split_input(lines: list) -> list:
    return [[x for x in line] for line in lines]


def get_rate(rate: str, vals: list) -> int:
    idx = 0 if rate == "gamma" else 1  # keep most common for gamma
    col_vals = [[x[i] for x in vals] for i in range(len(vals[0]))]
    col_counters = [Counter(x).most_common()[idx][0] for x in col_vals]
    return int(''.join(col_counters), 2)


def get_filtered_rate(rate: str, vals: list) -> int:
    idx = 0 if rate == "oxygen" else 1  # keep most common for oxygen
    keep = '1' if rate == "oxygen" else '0'
    col = 0
    while len(vals) > 1:
        c = Counter([x[col] for x in vals])
        filter_by = keep if c['1'] == c['0'] else c.most_common()[idx][0]
        vals = [x for x in filter(lambda x: x[col] == filter_by, vals)]
        col = col + 1
    return int(''.join(vals[0]), 2)


if __name__ == "__main__":
    lines = reader.get_lines()
    print("Part one: ", get_rate("gamma", lines) * get_rate("epsilon", lines))
    print("Part two: ", get_filtered_rate("oxygen", lines) * get_filtered_rate("co2", lines))
